import React from "react";
import ReactDOM from "react-dom";
import App from "./src/app"

import 'bootstrap/dist/css/bootstrap.min.css';
import './public/assets/scss/main.scss';
const selector = document.querySelector("#app");

import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

import { library } from '@fortawesome/fontawesome-svg-core'
import {
    faUser,
    faPowerOff,
    faChevronDown,
    faTerminal,
    faMoneyCheckAlt
} from '@fortawesome/free-solid-svg-icons'

import App from './app'
import { persistor, store } from './utils/store'

library.add(faUser, faPowerOff, faChevronDown, faTerminal, faMoneyCheckAlt)

ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <App />
        </PersistGate>
    </Provider>,
    document.getElementById('root')
)

ReactDOM.render(<App />	, selector);
